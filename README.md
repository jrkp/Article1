IF.zip provides a case setup for simulation of stream function waves using interFoam
IIF.zip provides a case setup for simulation of stream function waves using interIsoFoam
The case setups were used in the paper:
Qwist J.R.K. and Christensen E.D. (2022), Explicit Direct Surface Description for free surface flows
in the OpenFOAM CFD library, Int. J. Numer. Methods Fluids. (Under review)